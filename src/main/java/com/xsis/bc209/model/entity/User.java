package com.xsis.bc209.model.entity;

import com.xsis.bc209.model.CommonEntity;
import lombok.Data;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import java.util.List;

@Entity
@Table(name=User.TABLE_NAME)
@Data
public class User extends CommonEntity {
    public static final String TABLE_NAME = "t_user";

    @Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.TABLE, generator = TABLE_NAME)
    @TableGenerator(name = TABLE_NAME, table = "T_SEQUENCE", pkColumnName = "SEQ_NAME", pkColumnValue = TABLE_NAME, valueColumnName = "SEQ_VAL", allocationSize = 1, initialValue = 1)
    private String id;

    @Column(name = "email", nullable = false, unique = true)
    private String email;

    @Column(name = "password", nullable = false)
    private String password;

    @Column(name = "nohp", unique = true, nullable = false)
    private String nohp;

    @Column(name = "enabled")
    private Boolean enabled;

    @Column(name = "token_expired")
    private Boolean tokenExpired;

    @ManyToMany
    @JoinTable(
            name = "users_roles",
            joinColumns = @JoinColumn(
                    name = "user_id", referencedColumnName = "id"),
            inverseJoinColumns = @JoinColumn(
                    name = "role_id", referencedColumnName = "id"))
    private List<Role> roles;


    @OneToOne
    @JoinColumn(name = "detailid", referencedColumnName = "id")
    private UserDetail userDetail;

}
