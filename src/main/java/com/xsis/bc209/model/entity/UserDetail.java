package com.xsis.bc209.model.entity;

import com.xsis.bc209.model.CommonEntity;
import lombok.Data;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import java.util.Date;

@Data
@Entity
@Table(name = UserDetail.TABLE_NAME)
public class UserDetail extends CommonEntity {
    public static final String TABLE_NAME = "t_user_detail";

    @Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.TABLE, generator = TABLE_NAME)
    @TableGenerator(name = TABLE_NAME, table = "T_SEQUENCE", pkColumnName = "SEQ_NAME", pkColumnValue = TABLE_NAME, valueColumnName = "SEQ_VAL", allocationSize = 1, initialValue = 1)
    private String id;

    @Column(name = "name")
    private String name;

    @Column(name = "shortname")
    private String shortName;

    @Column(name = "tempatlahir")
    private String tempatLahir;

    @Column(name = "tanggallahir")
    @Temporal(TemporalType.TIMESTAMP)
    private Date tanggalLahir;

    @Column(name = "religion")
    private String religion;

    @Column(name = "jeniskelamin")
    private String jenisKelamin;

    @Column(name = "address")
    private String address;
}
